#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 30 10:03:15 2018

@author: ktp
"""

import os


RCOM_APPLOG_HOST = os.getenv("RCOM_APPLOG_HOST", "http://applog2-dev.dmp.true.th:8888/gelf") # default to dev
