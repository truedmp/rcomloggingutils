#rcom-logging-utils
utils for logging to AppLog

#Versions
1.1.1

#Changes

Version 1.1.1
    - The POST call doesn't create a daemon thread anymore. Instead, we use 
      requests-futures extension of requests to create a thread / connection 
      pool and send requests using those background threads instead. This 
      improves stability, performance and decouples the application from AppLog
      server being available. 

Version 1.1.0
   - AppLogger class doesn't extend Logger anymore. Instead it holds a reference
     to an appropriate logger. This ensure logging messages will be passed on to
     any log handlers used by Logger correctly. Basically you can be sure log
     messages will at least appear on console log. 
   - The POST call to AppLog server becomes a non-blocking daemon thread. If the
     AppLog server disconnects, the application will still be able to run 
     normally without crashing. 

Version 1.0.3
   - Add optional parameter for AppLogger constructor, "handlers" to allow 
     user-supplied handlers to be used. 
   - Ensure all AppLogger instances are provided with console log handler 
