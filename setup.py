from setuptools import setup, find_packages

REQUIRED_PACKAGES = [
    'requests==2.18.4',
    'requests-futures==0.9.9'
]

setup(name='rcomloggingutils',
      version='1.1.1',
      packages=find_packages(exclude=('tests',)),
      url='https://bitbucket.org/truedmp/rcomloggingutils.git',
      include_package_data=True,
      description='Rcom common utils for logging to Applog',
      author='Ktawut T.Pijarn',
      author_email='ktawut_tap@truecorp.co.th',
      install_requires=REQUIRED_PACKAGES,
      zip_safe=False)

